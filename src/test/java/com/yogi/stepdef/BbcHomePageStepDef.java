package com.yogi.stepdef;

import com.yogi.pageobjects.BbcHomePage;
import com.yogi.selenium.Driver;
import com.yogi.springconfig.BaseStepDef;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by YOGI on 24/09/2017.
 */
public class BbcHomePageStepDef extends BaseStepDef{
    @Autowired
    BbcHomePage bbcHomePage;

    @Given("^I am on bbc home page$")
    public void i_am_on_bbc_home_page() throws Throwable {
        Driver.getDriver();
    }

    @When("^I search \"(.*?)\" on bbc page$")
    public void i_search_on_bbc_page(String searchText) throws Throwable {
        bbcHomePage.goTo().searchFor(searchText);
    }

    @Then("^I should navigate to the \"(.*?)\" page$")
    public void i_should_navigate_to_the_page(String searchText) throws Throwable {
        Assert.assertTrue(bbcHomePage.goTo().VarifyPageTitle().contains(searchText));
        Driver.closeBrowser();
    }

}
