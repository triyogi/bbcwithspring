package com.yogi.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by YOGI on 24/09/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:cucumber/"
,glue = {"classpath:"}
//,dryRun = true
)
public class CucumberRunner {
}
