package com.yogi.springconfig;

import org.springframework.test.context.ContextConfiguration;

/**
 * Created by YOGI on 24/09/2017.
 */
@ContextConfiguration(classes = {TestPageObjectsConfig.class})
public class BaseStepDef {
}
