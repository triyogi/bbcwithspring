package com.yogi.springconfig;

import com.yogi.pageobjectcommands.BbcHomePageCommands;
import com.yogi.pageobjects.BbcHomePage;
import org.springframework.context.annotation.Bean;

/**
 * Created by YOGI on 24/09/2017.
 */
public class TestPageObjectsConfig {

    @Bean
    public BbcHomePage bbcHomePage(){
        return new BbcHomePage();
    }
}
