package com.yogi.selenium;

import com.yogi.common.BaseSteps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by YOGI on 24/09/2017.
 */
public class Driver {
    private static WebDriver driver;

    private Driver(){}

    public static WebDriver getDriver(){
        if(driver==null){
          System.setProperty("webdriver.chrome.driver","chromedriver.exe");
          driver=new ChromeDriver();
          driver.get(new BaseSteps().getProperty("baseUrl"));
          driver.manage().window().maximize();
        }
        return driver;
    }

    public static void closeBrowser(){
        if(driver!=null)
        driver.quit();
        driver=null;
    }
}
