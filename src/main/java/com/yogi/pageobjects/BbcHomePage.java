package com.yogi.pageobjects;

import com.yogi.pageobjectcommands.BbcHomePageCommands;

/**
 * Created by YOGI on 24/09/2017.
 */
public class BbcHomePage {

    public static BbcHomePageCommands goTo(){
        return new BbcHomePageCommands();
    }
}
