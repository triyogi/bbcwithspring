package com.yogi.pageobjectcommands;

import com.yogi.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Created by YOGI on 24/09/2017.
 */
public class BbcHomePageCommands {
    public void searchFor(String searchText) throws InterruptedException {

        Driver.getDriver().findElement(By.id("orb-search-q")).sendKeys(searchText);
        Driver.getDriver().findElement(By.id("se-searchbox-input-field")).sendKeys(Keys.ENTER);
    }

    public String VarifyPageTitle() {
        return Driver.getDriver().getTitle();
    }
}
